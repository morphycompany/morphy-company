<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Produtos extends Model
{
    protected $fillable = [ 'id',
                            'nome',
                            'descricao',
                            'tamanho',
                            'preco'];

    protected $table = 'Produtos';

    public function produtosVendas(){
        return $this->hasMany(produtosvenda::class, 'produto_id');
    }
}
