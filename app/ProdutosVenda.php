<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProdutosVenda extends Model
{
    protected $fillable = [ 'id',
				            'venda_id',
				            'produto_id',
	                        'quantidade',
                            'valor'];

    protected $table = 'ProdutosVenda';

    public function produtos(){
        return $this->belongsTo(produtos::class, 'produto_id');
    }
    public function vendas()
    {
        return $this->belongsTo(vendas::class, 'venda_id');
    }
}   
